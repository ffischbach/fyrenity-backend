FROM openjdk:17-alpine
Volume /tmp
ADD /target/*.jar fyrenity-backend-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/springbootgitlab-0.0.1-SNAPSHOT.jar"]
